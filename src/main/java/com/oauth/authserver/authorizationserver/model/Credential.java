package com.oauth.authserver.authorizationserver.model;

import lombok.Data;

@Data
public class Credential {

    private String username;

    private String password;
}
