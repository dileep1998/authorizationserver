package com.oauth.authserver.authorizationserver.config;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.oauth.authserver.authorizationserver.model.Usera;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class UserPrincipal implements UserDetails {

    private static final Logger LOG = LoggerFactory.getLogger(UserPrincipal.class);

    private static final long serialVersionUID = 1L;

    private Integer id;

    @JsonIgnore
    private String email;

    @JsonIgnore
    private String password;

    private String firstName;

    private Collection<? extends GrantedAuthority> authorities;

    private boolean accountNonExpired;

    private boolean accountNonLocked;

    private boolean credentialNonExpired;

    private boolean enabled;

    public UserPrincipal(Integer id, String firstName, String email, String password,
                         Collection<? extends GrantedAuthority> authorities, boolean accountNonLocked, boolean accountNonExpired, boolean credentialsNonExpired, boolean enabled) {

        this.id = id;

        this.email = email;

        this.password = password;

        this.firstName = firstName;

        this.authorities = authorities;

        this.accountNonExpired = accountNonExpired;

        this.accountNonLocked = accountNonLocked;

        this.credentialNonExpired = credentialsNonExpired;

        this.enabled = enabled;

    }


    public static UserPrincipal create(Usera user) {
        LOG.info("Enter static create()");
        List<GrantedAuthority> grantedAuthorities = user.getRoles().stream()
                .map(role -> new SimpleGrantedAuthority(role.getName())).collect(Collectors.toList());

        return new UserPrincipal(user.getId(), user.getUsername(), user.getEmail(),
                user.getPassword(), grantedAuthorities, user.isAccountNonExpired(),user.isAccountNonLocked(),user.isCredentialsNonExpired(),user.isEnabled());
    }

    public Integer getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }


    public String getUsername() {
        return firstName;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        UserPrincipal that = (UserPrincipal) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
