package com.oauth.authserver.authorizationserver.repository;

import com.oauth.authserver.authorizationserver.model.Usera;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserDetailRepository extends JpaRepository<Usera,Integer> {


    Optional<Usera> findByUsername(String name);

}
