package com.oauth.authserver.authorizationserver.service;

import com.oauth.authserver.authorizationserver.config.UserPrincipal;
import com.oauth.authserver.authorizationserver.model.Usera;
import com.oauth.authserver.authorizationserver.repository.UserDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service("userDetailsService")
public class UserDetailServiceImpl implements UserDetailsService {

    @Autowired
    private UserDetailRepository userDetailRepository;

    @Override
    public UserDetails loadUserByUsername(String name) {

        Usera optionalUser = userDetailRepository.findByUsername(name).orElseThrow(() -> new UsernameNotFoundException("USER_NOT_FOUND"));;

        return UserPrincipal.create(optionalUser);
    }

}
